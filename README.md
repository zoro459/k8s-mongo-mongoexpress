!['devops photo'](https://gitlab.com/zoro459/k8s-mongo-mongoexpress/-/raw/main/images/flow.bmp?ref_type=heads)

# Kubernetes Project: Connecting MongoDB and MongoDB Express Pods in security. 

Welcome to our guide on securely deploying MongoDB Express in a Kubernetes environment. This *reference* is tailored for DevOps  and developers seeking to ensure the secure deployment and operation of MongoDB Express while interacting with a MongoDB database.

## Project Overview

In this project, we have created a Kubernetes environment with two distinct pods:

1. **MongoDB Pod**: This pod hosts a MongoDB database instance. It is responsible for storing and managing data.

2. **MongoDB Express Pod**: This pod hosts a MongoDB Express web application. It provides a user-friendly interface to interact with the MongoDB database.

## Architecture

Our project's architecture involves the following components:

- **Pods**: We've set up two pods, each running in its own container:
  - `mongodb pod`: Contains the MongoDB database instance.
  - `mongoexpress pod`: Hosts the MongoDB Express web application.

- **Services**: Kubernetes Services allow communication between pods. We've created services to facilitate connectivity:
  - `mongodb service`: Exposes the MongoDB pod for communication.
  - `mongoexpress service`: Enables access to the MongoDB Express web app.

- **Secrets**: Secrets securely store sensitive information. We utilize secrets to store MongoDB credentials and access them within the pods.

- **ConfigMaps**: ConfigMaps hold configuration data. We've used ConfigMaps to provide the MongoDB Express pod with the MongoDB service URL.

> **Note**: If you want to change the MongoDB user and password, ensure to modify them in the `secret.yaml` file. Don't forget to encode the username and password.

## Getting Started

1. Clone this repository to your local machine.
2. `kubectl apply -f Secret.yaml`
3. `kubectl apply -f configmap.yaml`
4. `kubectl apply -f MongoDb.yaml`
5. `kubectl apply -f k8s-mongo-mongoexpress`
6. Monitor the pods' status using `kubectl get pods`.
7. Access the MongoDB Express web app using the external IP assigned to the `mongoexpress service`.

## Usage

- Use the MongoDB Express web app to interact with the MongoDB database.
- Secrets store the MongoDB username and password.
- ConfigMap provides the MongoDB Express pod with the MongoDB service URL.

## Customization

- Feel free to modify the MongoDB pod's configuration as needed.
- Update the Secret with your MongoDB credentials.
- Adjust the MongoDB Express web app's code or configuration if required.

## Feedback and Contributions

We welcome feedback, suggestions, and contributions! Feel free to open issues, submit pull requests, or reach out to us.

---

Have a question? Need help? Reach out to [my-linkedin](https://www.linkedin.com/in/mohamed-osama-789249278).
